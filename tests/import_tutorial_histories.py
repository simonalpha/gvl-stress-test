from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from gvl_test_base import GVLTestBase
from selenium_snippets.galaxy.history import History
from selenium_snippets.galaxy import snippet_base

history_list = [
                ("https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/RNAseqDGE_BASIC/Galaxy-History-RNAseqDGE_BASIC_Prep.tar.gz", "RNAseqDGE_BASIC_Prep"),
                ("https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/RNAseqDGE_BASIC/Galaxy-History-RNAseqDGE_BASIC_Complete.tar.gz", "RNAseqDGE_BASIC_Complete"),
                ("https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/variantCalling_BASIC/Galaxy-History-variantDetection_BASIC_Prep.tar.gz", "variantDetection_BASIC_Prep"),
                ("https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/variantCalling_BASIC/Galaxy-History-variantDetection_BASIC_Complete.tar.gz", "variantDetection_BASIC_Complete"),
                ("https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/Assembly/Galaxy-History-MicrobialAssembly_Prep.tar.gz", "MicrobialAssembly_Prep"),
                ("https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/Assembly/Galaxy-History-MicrobialAssembly_Complete.tar.gz", "MicrobialAssembly_Complete"),
                ("https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/RNASeq_ADVNCD/Galaxy-History-RNASeq-DGE_ADVANCED_Prep.tar.gz", "RNASeq-Advanced-Prep"),
                ("https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/RNASeq_ADVNCD/Galaxy-History-RNASeq-DGE_ADVANCED_Complete.tar.gz", "RNASeq_Advanced_Section7"),
                ("https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/variantCalling_ADVNCD/Galaxy-History-variantDetection_ADVANCED_Prep.tar.gz", "variantDetection_ADVANCED_Prep"),
                ("https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/variantCalling_ADVNCD/Galaxy-History-variantDetection_ADVANCED_Complete.tar.gz", "VariantDetection_ADVANCED_Section9")
               ]

class ImportTutorialHistories(GVLTestBase):

    def __init__(self, galaxy_test_context):
        super(ImportTutorialHistories, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    def execute_gvl_testcase(self):
        history = History(self.context)
        for item in history_list:
            history.import_history_archive(item[0])
        for item in history_list:
            history.publish_history_archive(item[1])


