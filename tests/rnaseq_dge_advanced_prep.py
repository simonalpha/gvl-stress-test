# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from gvl_test_base import GVLTestBase
from selenium_snippets.galaxy.history import History
from selenium_snippets.galaxy.rna_analysis import RNAAnalysis
from selenium_snippets.galaxy.sort_filter import SortFilter
from selenium_snippets.galaxy.sam_tools import SamTools
from selenium_snippets.galaxy.graph_display_data import GraphDisplayData
from selenium_snippets.galaxy.text_manipulation import TextManipulation
from selenium_snippets.galaxy.join_subtract_group import JoinSubtractGroup
from selenium_snippets.galaxy import snippet_base


class RNAseqDGEAdvancedPrep(GVLTestBase):

    def __init__(self, galaxy_test_context):
        super(RNAseqDGEAdvancedPrep, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    def execute_gvl_testcase(self):
        self.run_tute_section1()
        self.run_tute_section2()
        self.run_tute_section3()
        self.run_tute_section4()
        self.run_tute_section5()
        self.run_tute_section6()
        self.run_tute_section7()
        self.run_tute_section8()

    @snippet_base.ui_action()
    def run_tute_section1(self):
        driver = self.driver
        History(self.context).import_history("RNASeq-Advanced-Prep")

    @snippet_base.ui_action()
    def run_tute_section2(self):
        driver = self.driver
        rna_analysis = RNAAnalysis(self.context)
        sam_tools = SamTools(self.context)
        history = History(self.context)
        ref_genome = "Saccharomyces cerevisiae (sacCer2)"
        rna_analysis.run_tophat("1: batch1_chrI_1.fastq", ref_genome, paired_end="2: batch1_chrI_2.fastq")
        rna_analysis.run_tophat("3: batch2_chrI_1.fastq", ref_genome, paired_end="4: batch2_chrI_2.fastq")
        rna_analysis.run_tophat("5: batch3_chrI_1.fastq", ref_genome, paired_end="6: batch3_chrI_2.fastq")
        rna_analysis.run_tophat("7: chem1_chrI_1.fastq", ref_genome, paired_end="8: chem1_chrI_2.fastq")
        rna_analysis.run_tophat("9: chem2_chrI_1.fastq", ref_genome, paired_end="10: chem2_chrI_2.fastq")
        rna_analysis.run_tophat("11: chem3_chrI_1.fastq", ref_genome, paired_end="12: chem3_chrI_2.fastq")

        history.wait_for_datasets_to_finish()

        history.rename_history_item("Tophat for Illumina on data 2 and data 1: accepted_hits", "batch1: accepted_hits")
        history.rename_history_item("Tophat for Illumina on data 4 and data 3: accepted_hits", "batch2: accepted_hits")
        history.rename_history_item("Tophat for Illumina on data 6 and data 5: accepted_hits", "batch3: accepted_hits")
        history.rename_history_item("Tophat for Illumina on data 8 and data 7: accepted_hits", "chem1: accepted_hits")
        history.rename_history_item("Tophat for Illumina on data 10 and data 9: accepted_hits", "chem2: accepted_hits")
        history.rename_history_item("Tophat for Illumina on data 12 and data 11: accepted_hits", "chem3: accepted_hits")

        sam_tools.run_bam_to_sam("batch1: accepted_hits", include_header=True)
        history.rename_history_item("BAM-to-SAM on data", "batch1: sam")
        sam_tools.run_bam_to_sam("batch2: accepted_hits", include_header=True)
        history.rename_history_item("BAM-to-SAM on data", "batch2: sam")
        sam_tools.run_bam_to_sam("batch3: accepted_hits", include_header=True)
        history.rename_history_item("BAM-to-SAM on data", "batch3: sam")
        sam_tools.run_bam_to_sam("chem1: accepted_hits", include_header=True)
        history.rename_history_item("BAM-to-SAM on data", "chem1: sam")
        sam_tools.run_bam_to_sam("chem2: accepted_hits", include_header=True)
        history.rename_history_item("BAM-to-SAM on data", "chem2: sam")
        sam_tools.run_bam_to_sam("chem3: accepted_hits", include_header=True)
        history.rename_history_item("BAM-to-SAM on data", "chem3: sam")

        history.wait_for_datasets_to_finish()

    @snippet_base.ui_action()
    def run_tute_section3(self):
        RNAAnalysis(self.context).run_cuffdiff([{'name' : 'Batch', 'replicates' : ["batch1: accepted_hits", "batch2: accepted_hits", "batch3: accepted_hits"]},
                                                {'name' : 'Chem', 'replicates' : ["chem1: accepted_hits", "chem2: accepted_hits", "chem3: accepted_hits"]}
                                                ])
        SortFilter(self.context).run_sort_filter("gene differential expression testing", "c14=='yes'", lines_to_skip=1)
        History(self.context).rename_history_item("Filter on data", "CuffDiff - Significantly expressed genes")
        History(self.context).wait_for_datasets_to_finish()
        self.verify_section3_output()

    @snippet_base.ui_action()
    def verify_section3_output(self):
        diff_genes = self.count_diff_expressed_genes()
        if diff_genes != 53:
            raise Exception("Expected 53 differently expressed genes but found %s" % diff_genes)

    @snippet_base.ui_action()
    def count_diff_expressed_genes(self):
        History(self.context).view_last_history_item()
        self.switch_to_galaxy_content_frame()
        return len(self.driver.find_elements_by_xpath("id('content_table')//tr[normalize-space()]")) - 1

    @snippet_base.ui_action()
    def run_tute_section4(self):
        driver = self.driver
        rna_analysis = RNAAnalysis(self.context)
        rna_analysis.run_sam_bam_to_count_matrix(["batch1: sam", "batch2: sam", "batch3: sam", "chem1: sam", "chem2: sam", "chem3: sam"])
        History(self.context).wait_for_datasets_to_finish()

    @snippet_base.ui_action()
    def run_tute_section5(self):
        driver = self.driver
        rna_analysis = RNAAnalysis(self.context)
        rna_analysis.run_differential_count("bams to DGE count matrix_htseqsams2mx.xls", "Differential Counts - EdgeR",
                                            "Batch", ["batch1:_sam", "batch2:_sam", "batch3:_sam"],
                                            "Chem", ["chem1:_sam", "chem2:_sam", "chem3:_sam"], run_edge_r=True)
        SortFilter(self.context).run_sort_filter("DifferentialCountsEdgeR_topTable_edgeR.xls", "c6 <= 0.05", lines_to_skip=1)
        history = History(self.context)
        history.rename_history_item("Filter on data", "EdgeR - Significantly expressed genes")
        history.wait_for_datasets_to_finish()
        self.verify_section5_output()

    @snippet_base.ui_action()
    def verify_section5_output(self):
        diff_genes = self.count_diff_expressed_genes()
        if diff_genes != 58:
            raise Exception("Expected 58 differently expressed genes but found %s" % diff_genes)

    @snippet_base.ui_action()
    def run_tute_section6(self):
        driver = self.driver
        rna_analysis = RNAAnalysis(self.context)
        rna_analysis.run_differential_count("bams to DGE count matrix_htseqsams2mx.xls", "Differential Counts - Deseq",
                                            "Batch", ["batch1:_sam", "batch2:_sam", "batch3:_sam"],
                                            "Chem", ["chem1:_sam", "chem2:_sam", "chem3:_sam"], run_edge_r=False, run_deseq=True)
        SortFilter(self.context).run_sort_filter("DifferentialCountsDeseq_topTable_DESeq2.xls", "c6 <= 0.05", lines_to_skip=1)
        history = History(self.context)
        history.rename_history_item("Filter on data", "Deseq - Significantly expressed genes")
        history.wait_for_datasets_to_finish()
        self.verify_section6_output()

    @snippet_base.ui_action()
    def verify_section6_output(self):
        diff_genes = self.count_diff_expressed_genes()
        if diff_genes != 51:
            raise Exception("Expected 51 differently expressed genes but found %s" % diff_genes)

    @snippet_base.ui_action()
    def run_tute_section7(self):
        driver = self.driver
        history = History(self.context)
        SortFilter(self.context).run_sort_filter("CuffDiff - Significantly expressed genes", "c1 != 'test_id'", lines_to_skip=0)
        history.rename_history_item("Filter on data", "CuffDiff - Significantly expressed genes - noheader")
        SortFilter(self.context).run_sort_filter("EdgeR - Significantly expressed genes", "c1 != 'Name'", lines_to_skip=0)
        history.rename_history_item("Filter on data", "EdgeR - Significantly expressed genes - noheader")
        SortFilter(self.context).run_sort_filter("Deseq - Significantly expressed genes", "c1 != 'Contig'", lines_to_skip=0)
        history.rename_history_item("Filter on data", "Deseq - Significantly expressed genes - noheader")
        history.wait_for_datasets_to_finish()
        GraphDisplayData(self.context).run_proportional_venn("CommonSection", [{"input_file" : "CuffDiff - Significantly expressed genes - noheader", "col_index" : 2, "as_name" : "CuffDiff"},
                                                                               {"input_file" : "EdgeR - Significantly expressed genes - noheader", "col_index" : 0, "as_name" : "EdgeR"},
                                                                               {"input_file" : "Deseq - Significantly expressed genes - noheader", "col_index" : 0, "as_name" : "Deseq"}])
        self.verify_section7_output()

    @snippet_base.ui_action()
    def verify_section7_output(self):
        History(self.context).view_last_history_item()
        self.switch_to_galaxy_content_frame()
        if not self.is_element_present("/html/body/div/table/tbody/tr/td[preceding-sibling::td[contains(., 'CuffDiff ∩ EdgeR ∩ Deseq')]][contains(., '49')]"):
            raise Exception("Expected that CuffDiff ∩ EdgeR ∩ Deseq = 49")

    @snippet_base.ui_action()
    def run_tute_section8(self):
        driver = self.driver
        history = History(self.context)
        TextManipulation(self.context).run_cut("c3", "Tab", "CuffDiff - Significantly expressed genes - noheader")
        history.rename_history_item("Cut on data", "CuffDiff - column1")

        TextManipulation(self.context).run_cut("c1", "Tab", "EdgeR - Significantly expressed genes - noheader")
        history.rename_history_item("Cut on data", "EdgeR - column1")

        TextManipulation(self.context).run_cut("c1", "Tab", "Deseq - Significantly expressed genes - noheader")
        history.rename_history_item("Cut on data", "Deseq - column1")

        history.wait_for_datasets_to_finish()

        JoinSubtractGroup(self.context).run_compare_two_datasets("CuffDiff - column1", "EdgeR - column1")
        history.rename_history_item("Compare two Datasets", "Compare on CuffDiff and EdgeR")
        history.wait_for_datasets_to_finish()
        JoinSubtractGroup(self.context).run_compare_two_datasets("Compare on CuffDiff and EdgeR", "Deseq - column1")
        history.rename_history_item("Compare two Datasets", "Common significantly identified genes.")
        history.wait_for_datasets_to_finish()
        self.verify_section8_output()

    @snippet_base.ui_action()
    def verify_section8_output(self):
        diff_genes = self.count_diff_expressed_genes()
        if diff_genes != 49:
            raise Exception("Expected 49 differently expressed genes but found %s" % diff_genes)
