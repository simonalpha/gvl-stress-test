from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
import logging
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets.galaxy.login import Login
from selenium_snippets.galaxy import snippet_base

log = logging.getLogger(__name__)

class GVLTestBase(unittest.TestCase, SnippetBase):
    snippet_context = None
    driver_cleared = False

    def __init__(self, galaxy_test_context):
        unittest.TestCase.__init__(self)
        SnippetBase.__init__(self, galaxy_test_context)

    def runTest(self):
        try:
            self.driver.delete_all_cookies()
            self.begin_test()
        except Exception, e:
            self.context.error_count += 1
            log.exception("Error in runTest():")
            try:
                self.context.driver.save_screenshot("error.png")
            except Exception, escr:
                log.error("Error while saving screenshot...")
            raise e
        finally:
            log.debug("quitting Selenium driver...")
            try:
                self.driver.delete_all_cookies()
                self.driver.quit()
            except:
                pass
            self.driver_cleared = True
            self.clock_action('finish', 0)
            self.calculate_stats()

    @snippet_base.ui_action()
    def begin_test(self):
        Login(self.context).register_and_login(self.context.galaxy_server, self.context.galaxy_username, self.context.galaxy_email, self.context.galaxy_password)
        self.execute_gvl_testcase()

    # Intended to be overridden by child classes
    def execute_gvl_testcase(self):
        pass

    def tearDown(self):
        if not self.driver_cleared:
            try:
                log.warn("Driver still not cleared! Quitting again...")
                self.driver.delete_all_cookies()
                self.driver.quit()
                self.driver_cleared = True
            except:
                pass

# if __name__ == "__main__":
#    unittest.main()


