from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import functools
import logging

log = logging.getLogger(__name__)

class ActionType:
    ACTION_SEQUENCE = 1
    JOB_EXECUTE = 2

def ui_action(action_type=ActionType.ACTION_SEQUENCE):
    """
    This decorator logs each action and the type taken
    per action. It generates a tree structure of the actions
    based on their nesting. The action type determines the
    maximum timeout to wait for the action to complete.
    :type action_type: ActionType(Enum)
    :param action_type: An enum value indicating the action type. This is used to control timeout.
    """
    def wrap(func):
        """
        The actual wrapper
        """
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            obj = args[0]
            stored_timeout = obj.context.current_timeout
            try:
                if action_type == ActionType.JOB_EXECUTE:
                    obj.context.current_timeout = obj.context.job_timeout
                    obj.driver.implicitly_wait(obj.context.current_timeout)
                previous_action = obj._begin_action(func.__name__, action_type)
            except AttributeError:
                raise Exception("Decorated object does not define a _begin_action method."\
                                "Make sure that the object is of type SnippetBase.")
            ret = func(*args, **kwargs)
            obj.context.current_timeout = stored_timeout
            obj.driver.implicitly_wait(obj.context.current_timeout)
            obj._end_action(previous_action)
            return ret
        return wrapper
    return wrap

class SnippetBase(object):

    def __init__(self, snippet_context):
        self.snippet_context = snippet_context
        self.current_frame = None

    @property
    def driver(self):
        return self.snippet_context.driver

    @property
    def context(self):
        return self.snippet_context

    def clock_action(self, action_name, action_type):
        pass

    def _begin_action(self, action_name, action_type):
        log.debug(action_name + ", max-wait: " + str(self.context.current_timeout))
        new_action = {'action_name': action_name, 'action_start': time.time(), 'action_end' : None, 'child_actions' : [], 'action_type' : action_type}
        previous_action = self.context.current_action
        self.context.current_action.append(new_action)
        self.context.current_action = new_action['child_actions']
        return previous_action

    def _end_action(self, previous_action):
        previous_action[-1]['action_end'] = time.time()
        # print previous_action
        self.context.current_action = previous_action

    def wait_for_galaxy_content_frame(self):
        self.switch_to_galaxy_content_frame()
        # WebDriverWait(self.driver, 10).until(EC.frame_to_be_available_and_switch_to_it('galaxy_main'))

    def switch_to_galaxy_content_frame(self):
        if self.current_frame == 'galaxy_main':
            log.warn("Attempt to switch in succession to galaxy_main frame.. Ignoring...")
            return
        self.current_frame = 'galaxy_main'
        self.driver.switch_to_window(self.context.top_window)
        self.driver.switch_to_frame('galaxy_main')

    def switch_to_galaxy_outer_frame(self):
        if self.current_frame == 'outer':
            log.warn("Attempt to switch in succession to outer frame.. Ignoring...")
            return
        self.current_frame = 'outer'
        self.driver.switch_to_window(self.context.top_window)
        self.driver.switch_to_default_content()

    def switch_to_galaxy_history_frame(self):
        if self.current_frame == 'outer':
            log.warn("Attempt to switch in succession to outer/history frame.. Ignoring...")
            return
        self.current_frame = 'outer'
        self.driver.switch_to_window(self.context.top_window)
        # TODO: Backward compatibility
        if self.is_element_present("//iframe[@name='galaxy_history']"):
            WebDriverWait(self.driver, 10).until(EC.frame_to_be_available_and_switch_to_it('galaxy_history'))
        else:
            self.driver.switch_to_default_content()

    def _open_tool_node(self, elem_id, tool_name):
        driver = self.driver
        self.clock_action("%s click" % tool_name, 1001)
        self.switch_to_galaxy_outer_frame()
        driver.find_element_by_xpath("id('%s')/a/span" % elem_id).click()
        driver.find_element_by_link_text(tool_name).click()

    def _close_tool_node(self, elem_id, tool_name):
        driver = self.driver
        self.switch_to_galaxy_outer_frame()
        driver.find_element_by_xpath("id('%s')/a/span" % elem_id).click()

    def is_element_present(self, xpath):
        present = False
        try:
            self.driver.implicitly_wait(0)
            if WebDriverWait(self.driver, 1).until(EC.presence_of_element_located((By.XPATH, xpath))):
                present = True
            else:
                present = False
        except NoSuchElementException, e:
            present = False
        except TimeoutException, e:
            present = False
        self.driver.implicitly_wait(self.context.current_timeout)
        return present

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert.text
        finally:
            self.accept_next_alert = True

    def get_statistics(self):
        return self.context.statistics

    def calculate_stats(self):
        self.context.statistics = {"timing": self.context.timing,
                           "errors": self.context.error_count}

def navigate_to_tool(tool_elem_id, tool_name):
    """
    This decorator exists to make navigation to a given tool automatic
    :type tool_xpath: string
    :param tool_xpath: HTML element id of the tool's folder
    
    :type tool_name: string
    :param tool_name: The display name of the tool within the navigator
    """
    def wrap(func):
        """
        The actual wrapper
        """
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            obj = args[0]
            try:
                obj._open_tool_node(tool_elem_id, tool_name)
            except AttributeError:
                raise Exception("Decorated object does not define a navigate_to_tool method."\
                                "Make sure that the object is of type SnippetBase.")
            ret = func(*args, **kwargs)
            obj._close_tool_node(tool_elem_id, tool_name)
            return ret
        return wrapper
    return wrap

