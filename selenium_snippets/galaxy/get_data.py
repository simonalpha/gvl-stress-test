from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium_snippets.galaxy.history import History
from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets.galaxy import snippet_base

class GetData(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(GetData, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_getext", "Upload File")
    def run_upload_file(self, type, file_url, file_format=None):
        """Get Data->Upload file.
        Returns nothing
    
        :param type: Maybe either 'file', 'url' or 'text'
        :param file_url: Path to the file if type 'file', full url if type 'url' and context text if type 'text'
        :param file_format: (optional) Format of file.
    
        Usage::
    
          >>> from selenium_snippets.galaxy.get_data import GetData
          >>> req = GetData(self.context).run_upload_file('url', 'https://swift.rc.nectar.org.au:8888/v1/AUTH_377/public/variantCalling_BASIC/NA12878.GAIIx.exome_chr22.1E6reads.76bp.fastq')
        """

        driver = self.driver

        self.clock_action("Upload File btn click", 1001)
        self.switch_to_galaxy_content_frame()

        if type == 'file':
            driver.find_element_by_name("files_0|file_data").clear()
            driver.find_element_by_name("files_0|file_data").send_keys(file_url)
        elif type == 'url' or type == 'text':
            driver.find_element_by_name("files_0|url_paste").clear()
            driver.find_element_by_name("files_0|url_paste").send_keys(file_url)

        if file_format:
            driver.find_element_by_xpath("id('s2id_autogen1')/a[contains(@class, 'select2-choice')]").click()
            driver.find_element_by_xpath("//div[contains(@class, 'select2-drop')]/ul/li[contains(., '" + file_format + "')][last()]").click()

        driver.find_element_by_name("runtool_btn").click()
