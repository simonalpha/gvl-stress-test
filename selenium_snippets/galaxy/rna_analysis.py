from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
import unittest
import selenium.webdriver.support.ui as ui
import sys
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium_snippets.galaxy.snippet_base import SnippetBase
from selenium_snippets.galaxy.history import History
from selenium_snippets import util
from selenium_snippets.galaxy import snippet_base

class RNAAnalysis(SnippetBase):

    def __init__(self, galaxy_test_context):
        super(RNAAnalysis, self).__init__(galaxy_test_context)

    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_ngs-rna-tools", "Tophat for Illumina")
    def run_tophat(self, sequence_name, ref_genome, paired_end=None):
        driver = self.driver
        self.clock_action(sequence_name + " run_tool", 1001)
        self.switch_to_galaxy_content_frame()
        util.select_by_partial_text(driver.find_element_by_name("input1"), sequence_name)
        util.select_by_partial_text(driver.find_element_by_name("refGenomeSource|index"), ref_genome)
        if paired_end:
            Select(driver.find_element_by_name("singlePaired|sPaired")).select_by_visible_text("Paired-end")
            Select(driver.find_element_by_name("singlePaired|input2")).select_by_visible_text(paired_end)
        driver.find_element_by_name("runtool_btn").click()

    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_ngs-rna-tools", "Cuffdiff")
    def run_cuffdiff(self, condition_list, normalization_method=None, dispersion_method=None):
        driver = self.driver
        self.switch_to_galaxy_content_frame()
        self.clock_action("cuff diff run btn click", 1001)

        for idx, elem in enumerate(condition_list):
            self._add_cuffdiff_condition(idx, elem['name'], elem['replicates'])

        if normalization_method:
            Select(driver.find_element_by_name("library_norm_method")).select_by_visible_text(normalization_method)

        if dispersion_method:
            Select(driver.find_element_by_name("dispersion_method")).select_by_visible_text(dispersion_method)

        driver.find_element_by_name("runtool_btn").click()

        # wait for cuff diff to finish
        self.clock_action("cuff diff complete", 1001)
        History(self.context).wait_for_datasets_to_finish()

    def _add_cuffdiff_condition(self, condition_no, name, selected_replicates):
        driver = self.driver
        # Since conditions 0 and 1 are already present, only add from condition 2 onwards
        if condition_no > 1:
            driver.find_element_by_name("conditions_add").click()

        element = driver.find_element_by_name("conditions_" + str(condition_no) + "|name")
        element.clear()
        element.send_keys(name)
        for idx, replicate in enumerate(selected_replicates):
            self._add_cuffdiff_replicate(condition_no, idx, replicate)

    def _add_cuffdiff_replicate(self, condition_no, replicate_no, selected_replicate):
        driver = self.driver
        # Since replicate 0 is already present, only add from replicate 1 onwards
        if replicate_no > 0:
            driver.find_element_by_name("conditions_" + str(condition_no) + "|samples_add").click()
        util.select_by_partial_text(driver.find_element_by_name("conditions_" + str(condition_no) + "|samples_" + str(replicate_no) + "|sample"), selected_replicate)

    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_ngs-rna-tools", "SAM/BAM to count matrix")
    def run_sam_bam_to_count_matrix(self, sam_file_list):
        driver = self.driver
        self.clock_action("sam_bam_to_count_matrix: run_tool", 1001)
        self.switch_to_galaxy_content_frame()
        if len(sam_file_list) > 0:
            util.select_by_partial_text(driver.find_element_by_name("firstsamf"), sam_file_list[0])
        if len(sam_file_list) > 1:
            util.select_by_partial_text(driver.find_element_by_name("secondsamf"), sam_file_list[1])
        if len(sam_file_list) > 2:
            for idx, value in enumerate(sam_file_list[2:]):
                util.select_by_partial_text(driver.find_element_by_name("samfiles_" + str(idx) + "|samf"), sam_file_list[idx + 2])

        driver.find_element_by_name("runtool_btn").click()


    @snippet_base.ui_action()
    @snippet_base.navigate_to_tool("title_ngs-rna-tools", "Differential_Count")
    def run_differential_count(self, input_name, output_title, treatment_name, treatment_list, control_name, control_list, run_edge_r=True, run_deseq=False):
        driver = self.driver
        self.clock_action("different_count: run_tool", 1001)
        self.switch_to_galaxy_content_frame()

        driver.find_element_by_xpath("//div/a[contains(@class, 'select2-choice')]").click()
        driver.find_element_by_xpath("//div[contains(@class, 'select2-drop')]/ul/li[contains(., '" + input_name + "')][last()]").click()

        driver.find_element_by_name("title").clear()
        driver.find_element_by_name("title").send_keys(output_title)

        driver.find_element_by_name("treatment_name").clear()
        driver.find_element_by_name("treatment_name").send_keys(treatment_name)

        for treat_col in treatment_list:
            driver.find_element_by_xpath("id('tool_form')//input[@name='Treat_cols'][following-sibling::label[contains(., '" + treat_col + "')]]").click()

        driver.find_element_by_name("control_name").clear()

        for control_col in control_list:
            driver.find_element_by_xpath("id('tool_form')//input[@name='Control_cols'][following-sibling::label[contains(., '" + control_col + "')]]").click()

        driver.find_element_by_name("control_name").send_keys(control_name)

        if run_edge_r:
            Select(driver.find_element_by_name("edgeR|doedgeR")).select_by_visible_text("Run edgeR")
        else:
            Select(driver.find_element_by_name("edgeR|doedgeR")).select_by_visible_text("Do not run edgeR")

        if run_deseq:
            Select(driver.find_element_by_name("DESeq2|doDESeq2")).select_by_visible_text("Run DESeq2")
        else:
            Select(driver.find_element_by_name("DESeq2|doDESeq2")).select_by_visible_text("Do not run DESeq2")

        driver.find_element_by_name("runtool_btn").click()
