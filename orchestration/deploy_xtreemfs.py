import time
import os
import boto
import argparse
import logging
from fabric.api import *
from fabric.tasks import execute
from fabric.contrib.files import append, sed

log = logging.getLogger(__name__)

DATACENTER_MAP = """datacenters=QH2,NCI,NP,MONASH,QLD,TAS,SA\n
    distance.QH2-NCI=465\n
    distance.QH2-NP=27\n
    distance.QH2-MONASH=20\n
    distance.QH2-QLD=1371\n
    distance.QH2-TAS=602\n
    distance.QH2-SA=653\n
    distance.NCI-NP=461\n
    distance.NCI-MONASH=460\n
    distance.NCI-QLD=941\n
    distance.NCI-TAS=863\n
    distance.NCI-SA=958\n
    distance.NP-MONASH=7\n
    distance.NP-QLD=1376\n
    distance.NP-TAS=579\n
    distance.NP-SA=679\n
    distance.MONASH-QLD=1373\n
    distance.MONASH-TAS=584\n
    distance.MONASH-SA=674\n
    distance.QLD-TAS=1790\n
    distance.QLD-SA=1598\n
    distance.TAS-SA=1165\n
    addresses.QH2=115.146.84.0/22\n
    addresses.NCI=130.56.248.0/21\n
    addresses.NP=115.146.92.0/22\n
    addresses.MONASH=118.138.240.0/21\n
    addresses.QLD=130.102.154.0/23\n
    addresses.TAS=144.6.224.0/20\n
    addresses.SA=130.220.208.0/20\n
    max_cache_size=100"""

@parallel
def install_xtreemfs_common():
    sudo("add-apt-repository 'deb http://download.opensuse.org/repositories/home:/xtreemfs/xUbuntu_14.04 ./'")
    sudo('wget -q http://download.opensuse.org/repositories/home:/xtreemfs/xUbuntu_14.04/Release.key -O - | sudo apt-key add -')
    sudo('apt-get update')
    sudo("apt-get install xtreemfs-client -y")
    sudo("apt-get install xtreemfs-server -y")
    sudo("apt-get install xfsprogs -y")
    sudo("echo apt-get install xfsprogs -y")
    sudo("echo $'127.0.0.1\t'$HOSTNAME >> /etc/hosts")
    append('/etc/hosts', "127.0.0.1\t$HOSTNAME", use_sudo=True, escape=False)

@parallel
def setup_disks():
    sudo('mkfs.xfs /dev/vdc')
    sudo('mkdir -p /export/')
    sudo("mount /dev/vdc /export/")
    sudo('mkdir /export/xtreemfs')
    sudo('chown xtreemfs /export/xtreemfs')
    append('/etc/fstab', '/dev/vdc\t/export\tauto\tdefaults,nobootwait,comment=cloudconfig\t0\t2', use_sudo=True)
    # sudo("echo $'/dev/vdc\t/export\tauto\tdefaults,nobootwait,comment=cloudconfig\t0\t2' >> /etc/fstab")

def setup_hub():
    sudo('mkdir -p /export/xtreemfs/dir/database', user='xtreemfs')
    sudo('mkdir -p /export/xtreemfs/dir/db-log', user='xtreemfs')
    sed('/etc/xos/xtreemfs/dirconfig.properties', 'babudb.baseDir = /var/lib/xtreemfs/dir/database', 'babudb.baseDir = /export/xtreemfs/dir/database', use_sudo=True)
    sed('/etc/xos/xtreemfs/dirconfig.properties', 'babudb.logDir = /var/lib/xtreemfs/dir/db-log', 'babudb.logDir = /export/xtreemfs/dir/db-log', use_sudo=True)

    sudo('mkdir -p /export/xtreemfs/mrc/database', user='xtreemfs')
    sudo('mkdir -p /export/xtreemfs/mrc/db-log', user='xtreemfs')
    sed('/etc/xos/xtreemfs/mrcconfig.properties', 'babudb.baseDir = /var/lib/xtreemfs/mrc/database', 'babudb.baseDir = /export/xtreemfs/mrc/database', use_sudo=True)
    sed('/etc/xos/xtreemfs/mrcconfig.properties', 'babudb.logDir = /var/lib/xtreemfs/mrc/db-log', 'babudb.logDir = /export/xtreemfs/mrc/db-log', use_sudo=True)

    append('/etc/xos/xtreemfs/datacentermap', DATACENTER_MAP, use_sudo=True)

    sudo("/etc/init.d/xtreemfs-dir start")
    time.sleep(5)  # Sleep a little till the dir service comes up
    sudo("/etc/init.d/xtreemfs-mrc start")

def setup_osds(hub):
    sudo('mkdir -p /export/xtreemfs/objs/', user='xtreemfs')
    sed('/etc/xos/xtreemfs/osdconfig.properties', 'dir_service.host = localhost', 'dir_service.host = {0}'.format(hub), use_sudo=True)
    sudo("/etc/init.d/xtreemfs-osd start")

def setup_replication(hub):
    sudo('mkdir -p /export/xtreemfs/objs/', user='xtreemfs')
    sed('/etc/xos/xtreemfs/osdconfig.properties', 'dir_service.host = localhost', 'dir_service.host = {0}'.format(hub), use_sudo=True)
    sudo("/etc/init.d/xtreemfs-osd start")

def deploy_xtreemfs_grid(hub, nodes):
    execute(install_xtreemfs_common, hosts=[hub] + nodes)
    execute(setup_disks, hosts=[hub] + nodes)
    execute(setup_hub, hosts=hub)
    execute(setup_osds, hub, hosts=nodes)
    execute(setup_replication, hub, hosts=hub)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-g", "--gridhub", type=str, help="xtreemFS head node", required=True)
    parser.add_argument('-n', '--nodes', nargs='+', type=str, help="xtreemfs OSD nodes", required=True)
    parser.add_argument('-k', '--keyfile', type=str, help="Keyfile location", required=True)
    args = parser.parse_args()

    env.user = "ubuntu"
    env.key_filename = args.keyfile
    deploy_xtreemfs_grid(args.gridhub, args.nodes)


