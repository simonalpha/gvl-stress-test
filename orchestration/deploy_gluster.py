import time
import os
import boto
import argparse
import logging
from fabric.api import *
from fabric.tasks import execute
from fabric.contrib.files import append

log = logging.getLogger(__name__)

@parallel
def install_gluster_common():
    sudo('add-apt-repository ppa:semiosis/ubuntu-glusterfs-3.6 -y')
    sudo('apt-get update')
    sudo("apt-get install glusterfs-server -y")
    sudo("apt-get install xfsprogs -y")

@parallel
def setup_disks():
    sudo('mkfs.xfs /dev/vdc')
    sudo('mkdir -p /export/')
    sudo("mount /dev/vdc /export/")
    append('/etc/fstab', '/dev/vdc\t/export\tauto\tdefaults,nobootwait,comment=cloudconfig\t0\t2', use_sudo=True)
    # sudo("echo $'/dev/vdc\t/export\tauto\tdefaults,nobootwait,comment=cloudconfig\t0\t2' >> /etc/fstab")

def setup_peers(peers):
    for peer in peers:
        sudo("gluster peer probe {0}".format(peer))
    sudo("gluster volume create gvl-vol-replicated stripe {0} {1}:/export/gvl-vol-replica {2}:/export/gvl-vol-replica".format(len(peers) + 1, env.host, ":/export/gvl-vol-replica ".join(peers)))
    sudo("gluster volume start gvl-vol-replicated")

def deploy_gluster_grid(nodes):
    execute(install_gluster_common, hosts=nodes)
    execute(setup_disks, hosts=nodes)
    execute(setup_peers, nodes[1:], hosts=nodes[0])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--nodes', nargs='+', type=str, help="PhantomJS nodes", required=True)
    parser.add_argument('-k', '--keyfile', type=str, help="Keyfile location", required=True)
    args = parser.parse_args()

    env.user = "ubuntu"
    env.key_filename = args.keyfile
    deploy_gluster_grid(args.nodes)


