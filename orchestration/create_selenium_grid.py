import time
import os
import boto
import argparse
import logging
from fabric.api import *
from fabric.tasks import execute

import deploy_selenium_grid as deploygrid
import create_nodes as nodectrl

LOG_CONFIG = {'version':1,
              'formatters': {
                            'default_formatter' : { 'format': '%(asctime)s : %(levelname)s : %(message)s' }
                            },
              'handlers': {'console': { 'class': 'logging.StreamHandler',
                                      'level': logging.DEBUG},
                          'file': {'class': 'logging.FileHandler',
                                   'filename': 'create_node.log',
                                   'formatter': 'default_formatter',
                                   'level': logging.DEBUG}},
              'root': {'handlers' : ('console',), 'level': logging.DEBUG }}
logging.config.dictConfig(LOG_CONFIG)
log = logging.getLogger(__name__)

def create_selenium_grid(hub_flavour, node_flavour, zone, instance_prefix, instance_count):
    log.debug("Creating selenium hub instance...")
    hub = nodectrl.create_instance(hub_flavour, zone, instance_prefix + "-hub")
    log.debug("Creating phantomjs worker instances...")
    instances = nodectrl.create_instances(node_flavour, zone, instance_prefix + "-node{0}", instance_count - 1)
    log.debug("Sleeping a bit till the ssh services come up...")
    time.sleep(20)  # Sleep a little till the ssh services come up
    log.debug("Deploying software...")
    deploygrid.deploy_selenium_grid(hub.private_ips[0], [instance.private_ips[0] for instance in instances])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--nodes', type=int, help="Number of nodes in grid", required=True)
    parser.add_argument('-k', '--keyfile', type=str, help="Keyfile location", required=True)
    parser.add_argument('-z', '--zone', type=str, help="Placement zone to create nodes", required=True)
    parser.add_argument('-m', '--master', type=str, help="Flavour of master node", required=True)
    parser.add_argument('-w', '--worker', type=str, help="Flavour of worker nodes", required=True)
    parser.add_argument('-p', '--prefix', type=str, help="Name prefix of grid.", required=True)
    args = parser.parse_args()

    env.user = "ubuntu"
    env.disable_known_hosts = True
    env.key_filename = args.keyfile
    create_selenium_grid(args.master, args.worker, args.zone, args.prefix, args.nodes)
