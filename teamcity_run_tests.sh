#!/bin/bash
virtualenv .
source bin/activate
pip install -r requirements.txt
python gvl_test_runner.py -c $GVL_ST_CLASS -s $GVL_ST_SERVER -pt $GVL_ST_PAGE_TIMEOUT -jt $GVL_ST_JOB_TIMEOUT -g $GVL_ST_GRID -u $GVL_ST_USER -p $GVL_ST_PWD -e $GVL_ST_EMAIL 